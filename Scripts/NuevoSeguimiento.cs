using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NuevoSeguimiento : MonoBehaviour
{
    public GameObject seguimiento, ventana, hitos;
    public Canvas registro;
    public float transformar;
    public DateTime fechaInicial;
    public bool banderita = true, banderito1 = true, banderito2 = true, banderito3 = true, banderito4 = true, sethitos = false;

    private void Start()
    {
        this.gameObject.name = "Panel";
        seguimiento = GameObject.Find("Panel");
    }

    public void GestionarSeguimiento()
    {
        if (this.gameObject.name.ToString().Equals("Panel"))
        {
            Instantiate(GameObject.Find("Panel"), GameObject.Find("Contenido").transform, false);

            GameObject.Find("Panel").name = "NuevoSeguimiento";
            GameObject.Find("Nestudiante").name = "estudiante";
            Destroy(GameObject.Find("panelMas"));
            GameObject.Find("seguimiento").name = "sestudiante";
            ventana = GameObject.Find("ScrollViewMain");
            ventana.SetActive(false);
            registro.enabled = true;
        }
        else
        {
            GameObject.Find("MainCanvas").GetComponent<Canvas>().enabled = false;
            hitos.GetComponent<Canvas>().enabled = true;
        }
    }

    private void Update()
    {
        if (!this.gameObject.name.Equals("Panel"))
        {
            sethitos = true;
            hitos = GameObject.Find("HitosCanvas" + this.gameObject.name);
            hitos.transform.GetChild(2).GetChild(0).name = this.gameObject.name;
        }

        if (banderita && !this.gameObject.name.Equals("Panel"))
        {
            banderita = false;
            fechaInicial = DateTime.Now;
        }

        if (fechaInicial.AddDays(15) <= DateTime.Now && banderito1 && !this.gameObject.name.Equals("Panel"))
        {
            if (PlayerPrefs.GetFloat(this.gameObject.name + "progH1") < 100)
            {
                banderito1 = false;
                transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("rflag");
                transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = Resources.Load<Sprite>("blueflag");
            }
            else
            {
                banderito1 = false;
                transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("gflag");
                transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = Resources.Load<Sprite>("blueflag");
            }
        }
        if (fechaInicial.AddDays(30) <= DateTime.Now && banderito2 && !this.gameObject.name.Equals("Panel"))
        {
            if (PlayerPrefs.GetFloat(this.gameObject.name + "progH2") < 100)
            {
                banderito2 = false;
                transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = Resources.Load<Sprite>("rflag");
                transform.GetChild(0).GetChild(2).GetComponent<Image>().sprite = Resources.Load<Sprite>("blueflag");
            }
            else
            {
                banderito2 = false;
                transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = Resources.Load<Sprite>("gflag");
                transform.GetChild(0).GetChild(2).GetComponent<Image>().sprite = Resources.Load<Sprite>("blueflag");
            }
        }

        if (fechaInicial.AddDays(45) <= DateTime.Now && banderito3 && !this.gameObject.name.Equals("Panel"))
        {
            if (PlayerPrefs.GetFloat(this.gameObject.name + "progH3") < 100)
            {
                banderito3 = false;
                transform.GetChild(0).GetChild(2).GetComponent<Image>().sprite = Resources.Load<Sprite>("rflag");
                transform.GetChild(0).GetChild(3).GetComponent<Image>().sprite = Resources.Load<Sprite>("blueflag");
            }
            else
            {
                banderito3 = false;
                transform.GetChild(0).GetChild(2).GetComponent<Image>().sprite = Resources.Load<Sprite>("gflag");
                transform.GetChild(0).GetChild(3).GetComponent<Image>().sprite = Resources.Load<Sprite>("blueflag");
            }

        }

        if (fechaInicial.AddDays(60) <= DateTime.Now && banderito4 && !this.gameObject.name.Equals("Panel"))
        {
            if (PlayerPrefs.GetFloat(this.gameObject.name + "progH4") < 100)
            {
                banderito4 = false;
                transform.GetChild(0).GetChild(3).GetComponent<Image>().sprite = Resources.Load<Sprite>("rflag");
            }
            else
            {
                banderito4 = false;
                transform.GetChild(0).GetChild(3).GetComponent<Image>().sprite = Resources.Load<Sprite>("gflag");
            }
        }
    }

}
